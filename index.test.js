import RssParse from './'

import MOCK_DATA from './mock.data.js'

describe('RssData', () => {
  const rssData = RssParse(MOCK_DATA)

  it('Returns Feed Items', () => {
    let Unit = rssData
      .itemSet()
      .itemGet()

    expect(Unit.length).toBeGreaterThan(0)
    expect(typeof Unit[0] === 'string').toBeTruthy()
  })

  it('Returns Feed Content', () => {
    let Unit = rssData
      .itemSet()
      .contentSet(['link', 'title'])
      .contentGet()

    expect(Unit.length).toBeGreaterThan(0)
    expect(Unit[0].title).toBeTruthy()
    expect(Unit[0].link).toBeTruthy()
    expect(typeof Unit[0] === 'object').toBeTruthy()
  })

  it('Clears CDATA', () => {
    let Unit = rssData
      .itemSet()
      .contentSet(['link', 'title'])
    let Unit_contentBefore = Unit.contentGet()
    let Unit_contentAfter = Unit.clearCdata('title').contentGet()

    expect(Unit_contentBefore.length).toBeGreaterThan(0)
    expect(Unit_contentBefore[0].title).toMatch(/CDATA/)
    expect(Unit_contentAfter[0].title).not.toMatch(/CDATA/)
  })

  it('Retreives Links', () => {
    let Unit = rssData
      .itemSet()
      .contentSet(['link', 'title'])
      .linksSet('url=')
      .contentGet()

    expect(Unit.length).toBeGreaterThan(0)
    expect(Unit[0].links).toBeTruthy()
    expect(Unit[0].links[0]).toMatch(/Fires All Of His Employees/)
  })
})
